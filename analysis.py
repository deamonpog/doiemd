import pandas as pd
import numpy as np
import matplotlib.pyplot as plt



def analyzeCascade(thisRoot):
    this_event_time_user = df[df['conversationID'] == thisRoot][['time','userID']].reset_index(drop=True)
    adopted_users = set()
    adoption_series = [0 for i in range(this_event_time_user.shape[0])]
    for i,v in this_event_time_user.iterrows():
        if v.userID in adopted_users:
            adoption_series[i] = adoption_series[i-1]
        else:
            adopted_users.add(v.userID)
            adoption_series[i] = adoption_series[i-1] + 1

    this_event_time_user['adoption'] = adoption_series
    this_event_time_user['cascadeSize'] = list(range(1,this_event_time_user.shape[0]+1))
    print(this_event_time_user)

    this_event_time_user.set_index('time')[['adoption','cascadeSize']].plot()
    plt.show()

if __name__ == "__main__":
    df = pd.read_csv("/home/social-sim/Desktop/SSDATA/CP6/DryRunA/cp6_all_data_extracted_macm.csv", parse_dates=['time'])

    platforms = list(df.platform.unique())

    thisPlatform = 'jamii'

    roots = df[df.platform == thisPlatform].conversationID.unique()

    analyzeCascade(roots[0])
