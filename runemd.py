# Import EvolutionaryModelDiscovery
from EvolutionaryModelDiscovery import EvolutionaryModelDiscovery
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import warnings
from sklearn.exceptions import DataConversionWarning
warnings.filterwarnings(action='ignore', category=DataConversionWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)
pd.options.mode.chained_assignment = None  # default='warn'

NUM_BINS = 10
BETA = 0.59
AGENT_COUNT = 150

#groundTruth_df = pd.read_csv("/home/social-sim/RESEARCH/DOI_Inverse/doiemd/outputs/anti_58_of_163__binned_10x10.csv")
#groundTruth_df = pd.read_csv("./outputs/anti_infrastructure_46_of_163__binned_10x10.csv")
groundTruth_df = pd.read_csv('./outputs/covid_119_of_163__binned_10x10.csv')
groundTruth_df = groundTruth_df[['timebinMid','valbinMid']]
#groundTruth_df = groundTruth_df[groundTruth_df['timebinMid'] != 0]
groundTruth_df = groundTruth_df.groupby('timebinMid').median()['valbinMid'].reset_index()

init_beta = BETA
init_infects = groundTruth_df.iloc[0]['valbinMid']

print(f"Beta : {init_beta}")
print(f"Initial Infects : {init_infects}")
print("GT:\n",groundTruth_df)

#groundTruth_df.set_index('timebinMid').plot(label='median', color='red')
#plt.xticks(np.arange(0.05, 1.0, 0.1))
#plt.yticks(np.arange(0.05, 1.0, 0.1))
#plt.grid(True)
#plt.legend()
#plt.show()

groundTruth_df.set_index("timebinMid", inplace=True)
indexLabels = list(pd.cut([],bins=np.linspace(0, 1, 1 + NUM_BINS)).categories.mid)

# Provide the model path
modelPath = "DOI_one_function.nlogo"
# List the setup commands. In this case we just execute the model's setup procedure
setup = [f"one-setup {AGENT_COUNT} {init_beta} {init_infects}"]
# Provide measurement reporters to evaluate the simulations
measurements = ["max-in-degree","avg-in-degree","ticks", "infectedFraction"]
# Specify how many ticks you want to run each simulation for.
ticks = 1000
# Use the above information to initialize EvolutionaryModelDiscovery
emd = EvolutionaryModelDiscovery(netlogo_path="C:\\Program Files\\NetLogo 6.0.4\\", 
                                 model_path=modelPath, 
                                 setup_commands=setup, 
                                 measurement_reporters=measurements, 
                                 ticks_to_run=ticks)


def calculateMAPE(actual_df, pred_df, indexList, initValue, colName):
    minIndex = min(actual_df.iloc[0].name, pred_df.iloc[0].name)
    #print("MinIndex : ",minIndex)
    current_actual_val = initValue
    current_pred_val = initValue
    mape_vals = []
    for n in indexList:
        if(n > minIndex):
            current_actual_val = actual_df.loc[n]["valbinMid"] if (n in actual_df.index) else (current_actual_val)
            current_pred_val = pred_df.loc[n]["valbinMid"] if (n in pred_df.index) else (current_pred_val)
            ape_val = abs((current_actual_val - current_pred_val) / current_actual_val)
            #print(f"name:{n}\tA:{current_actual_val} -> \tP:{current_pred_val} \t== {ape_val}")
            mape_vals.append(ape_val)
    #print("APE VAL LIST:\n",mape_vals)
    mape = np.mean(mape_vals)
    #print("MAPE = ",mape)
    return mape

def calcSampledCurve(resultdf, valColName, timeColName, timeNumBins, valNumBins):
        resultdf['timebin'] = pd.cut(resultdf[timeColName],bins=np.linspace(0, 1, 1 + timeNumBins))
        resultdf['valbin'] = pd.cut(resultdf[valColName],bins=np.linspace(0, 1, 1 + valNumBins))
        #print(resultdf['timebin'].cat.categories)
        #print(resultdf['valbin'].cat.categories)
        #print(resultdf)
        sampled_df = resultdf[['timebin','valbin']]
        sampled_df = sampled_df.drop_duplicates()
        sampled_df['timebinMid'] = sampled_df['timebin'].apply(lambda x: x.mid).astype('float')
        sampled_df['valbinMid'] = sampled_df['valbin'].apply(lambda x: x.mid).astype('float')
        #sampled_df.set_index('timebinMid')['valbinMid'].plot()
        return sampled_df
        

def MAPE_ValueCalculation(results):
    #print(results.shape)
    #print(results)
    lastValue = results.iloc[-1].infectedFraction
    lastTick = 0
    #print("lastValue: {} at {}".format(lastValue, lastTick))
    for i in range(-1, -1-results.shape[0], -1):
        #print("{}:\t{}:\t{}".format(i, i + results.shape[0], results.iloc[i].infectedFraction))
        if results.iloc[i].infectedFraction != lastValue:
            lastTick = i + results.shape[0] + 1
            #print(results.iloc[i].infectedFraction)
            break
    #print("last value {} at tick : {}".format(lastValue, lastTick))
    results['tickfrac'] = results.ticks.apply(lambda x: x / (lastTick + 1))
    results = results[results.ticks <= lastTick + 1]
    #print("before calc: \n",results)
    results = calcSampledCurve(results, 'infectedFraction', 'tickfrac', NUM_BINS, NUM_BINS)
    #print("after calc: \n",results)
    results = results.groupby("timebinMid").max()
    return calculateMAPE(groundTruth_df, results, indexLabels, init_infects, 'valbinMid')


print("setting meta values")

emd.set_objective_function(MAPE_ValueCalculation)

emd.set_mutation_rate(0.05)
emd.set_crossover_rate(0.8)
emd.set_generations(50)
emd.set_population_size(50)
emd.set_depth(1,10)
emd.set_is_minimize(True)

if __name__ == '__main__':
    #dfdummy = pd.read_csv("/home/social-sim/RESEARCH/DOI_Inverse/doiemd/outputs/anti_covid_debt_12_of_163__binned_10x10.csv")
    #dfdummy = dfdummy[['timebinMid','valbinMid']]
    #dfdummy = dfdummy[dfdummy['timebinMid'] != 0]

    #print("-------DUMMY--------")
    #print(dfdummy.groupby('timebinMid').max()['valbinMid'])

    #print("=======GROUND TRUTH=========")
    #print(groundTruth_df.set_index('timebinMid')['valbinMid'])

    #groundTruth_df.set_index('timebinMid')['valbinMid'].plot(label="gt")
    #df_gt_max = groundTruth_df.groupby('timebinMid').max()['valbinMid']

    #df_gt_max.plot(label="gt_max")
    #print("=======GROUND TRUTH MAX=========")
    #print(df_gt_max)

    #dfdummy.set_index('timebinMid')['valbinMid'].plot(label="dummy")
    #df_dummy_max = dfdummy.groupby('timebinMid').max()['valbinMid']

    #df_dummy_max.plot(label="dummy_max")
    #print("-------DUMMY MAX--------")
    #print(df_dummy_max)

    #print("MAPE: ", mape(df_gt_max, df_dummy_max))

    #plt.legend()
    #plt.show()

    print("Starting evolve")
    emd.evolve()
    fi = emd.get_factor_importances_calculator("FactorScores.csv")
    GI = fi.get_gini_importances(interactions=True)
    PI = fi.get_permutation_accuracy_importances(interactions=True)
    print(GI)
    print(PI)
    GI.to_csv("GI.csv")
    PI.to_csv("PI.csv")

