# Import EvolutionaryModelDiscovery
from EvolutionaryModelDiscovery import EvolutionaryModelDiscovery
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Provide the model path
modelPath = "emd_test.nlogo"
# List the setup commands. In this case we just execute the model's setup procedure
setup = [f"setup"]
# Provide measurement reporters to evaluate the simulations
measurements = ["ticks"]
# Specify how many ticks you want to run each simulation for.
ticks = 3
# Use the above information to initialize EvolutionaryModelDiscovery
emd = EvolutionaryModelDiscovery(netlogo_path="/opt/netlogo/", 
                                 model_path=modelPath, 
                                 setup_commands=setup, 
                                 measurement_reporters=measurements, 
                                 ticks_to_run=ticks)

def averageNumberOfAgents(results):
    return results.shape[0]

emd.set_objective_function(averageNumberOfAgents)

emd.set_mutation_rate(0.05)
emd.set_crossover_rate(0.8)
emd.set_generations(2)
emd.set_population_size(2)
emd.set_depth(1,20)
emd.set_is_minimize(True)

if __name__ == '__main__':
    emd.evolve()
    fi = emd.get_factor_importances_calculator("FactorScores.csv")
    GI = fi.get_gini_importances(interactions=True)
    PI = fi.get_permutation_accuracy_importances(interactions=True)
    print(GI)
    print(PI)
    GI.to_csv("GI.csv")
    PI.to_csv("PI.csv")

