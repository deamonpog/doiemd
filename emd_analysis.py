import pandas as pd
import matplotlib.pyplot as plt

factorscores_file = '/home/social-sim/RESEARCH/DOI_Inverse/doiemd/FactorScores.csv'

df = pd.read_csv(factorscores_file)
other_columns = {'Run', 'Gen', 'Rule', 'Fitness'}
factor_columns_list = set(list(df.columns)).difference(other_columns)

title_dict = {'intensityasconstantprobabilityoverinfected' : 'Intensity as Prob. over Infected',
'constbyavgnx' : 'Average Size of Neighborhood',
'intensitybysizeofix' : 'Intensity as Size of Infected Neighborhood',
'intensityasfractinfectednbrs' : 'Intensity as Fraction of Infected Neighbors',
'calcproductoverinfected' : 'Product over Infected Neighbors',
'constbymaxnx': 'Max Size of Neighborhood',
'fractbyrand':'Random Number',
'constbynx':'Size of Neighborhood'
}

for factor_column in factor_columns_list:
    print(factor_column)
    t = 0
    def plotBox(axarray, x):
        global t
        if x.shape[0] > 100:
            t += 1
            axarray[t-1].boxplot(x, positions=[x.name])

    # number of values with more than 100 samples
    num_of_values = df[factor_column].value_counts()[df[factor_column].value_counts() > 100].shape[0]

    fig , axes = plt.subplots(1, num_of_values, sharey=True)
    plt.suptitle(f"{title_dict[factor_column]}")
    df.groupby(factor_column)['Fitness'].apply( lambda x: plotBox(axes,x) )
    plt.savefig(f"{factor_column}.png")
    plt.show()

